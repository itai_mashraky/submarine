﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AI_Controller : MonoBehaviour
{
    int[,] playerBoard = new int[10, 10]; // הלוח עם הצוללות של השחקן
    int[,] playerSecondBoard = new int[10, 10]; // הלוח ניסיונות של המחשב
    int[,] aiBoard = new int[10, 10]; // הלוח עם הצוללות של המחשב
    int[,] aiSecondBoard = new int[10, 10]; // הלוח ניסיונות של השחקן
    int[,] helpBoard = new int[10, 10]; // מערך ריק    
    [SerializeField] private PlayerController PlayerControllerScript;
    [SerializeField] private string ans = "Didnt hit a ship befor";
    int computerRow = 0;
    int computerLine = 0;
    [SerializeField] private int count = 0;
    [SerializeField] private GameManager GameManagerScript;
    [SerializeField] private UI_Manager UI_Manager_Script;

    // Start is called before the first frame update
    void Start()
    {
        PlayerControllerScript = GameObject.Find("PlayerController").GetComponent<PlayerController>();
        GameManagerScript = GameObject.Find("GameManager").GetComponent<GameManager>();
        AI(5);
        AI(4);
        AI(3);
        AI(3);
        AI(2);
        UI_Manager_Script = GameObject.Find("Canvas").GetComponent<UI_Manager>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void FillPlayermap()
    {
        Node[,] tempArray = PlayerControllerScript.GetNodes();

        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if (tempArray[i, j].GetIsEmpty())
                {
                    playerBoard[i, j] = 0;
                }
                else
                {
                    playerBoard[i, j] = 1;
                }
            }
        }

    }

    public void AI(int length)
    {
        System.Random rnd = new System.Random();
        bool temp1 = true;
        int XstartingPosition = 0;
        int YstartingPosition = 0;
        int[] tempArr = new int[4];
        string direction = "";
        string ans = "no";
        while (ans == "no") // משתנה בסוף
        {
            while (temp1)
            {
                XstartingPosition = rnd.Next(0, 10);
                YstartingPosition = rnd.Next(0, 10);
                if (aiBoard[XstartingPosition, YstartingPosition] == 0)
                {
                    temp1 = false;
                }
            }
            int index = -1;
            temp1 = true;
            tempArr = RandomArr();
            while (temp1)
            {
                index++;
                if (index > 3)
                {
                    ans = "no";
                    break;
                }
                if (tempArr[index] == 1)
                {
                    direction = "Right";
                    if (CheckZeros(aiBoard, XstartingPosition, YstartingPosition, direction, length))
                    {
                        temp1 = false;
                        ans = "yes";
                        tempArr[index] = 0;
                    }
                }
                if (tempArr[index] == 2)
                {
                    direction = "Left";
                    if (CheckZeros(aiBoard, XstartingPosition, YstartingPosition, direction, length))
                    {
                        temp1 = false;
                        ans = "yes";
                        tempArr[index] = 0;
                    }
                }
                if (tempArr[index] == 3)
                {
                    direction = "Up";
                    if (CheckZeros(aiBoard, XstartingPosition, YstartingPosition, direction, length))
                    {
                        temp1 = false;
                        ans = "yes";
                        tempArr[index] = 0;
                    }
                }
                if (tempArr[index] == 4)
                {
                    direction = "Down";
                    if (CheckZeros(aiBoard, XstartingPosition, YstartingPosition, direction, length))
                    {
                        temp1 = false;
                        ans = "yes";
                        tempArr[index] = 0;
                    }
                }
            }
        }
        if (direction == "Right")
        {
            for (int i = 0; i < length; i++)
            {
                aiBoard[XstartingPosition, YstartingPosition + i] = 1;
            }
        }
        if (direction == "Left")
        {
            for (int i = 0; i < length; i++)
            {
                aiBoard[XstartingPosition, YstartingPosition - i] = 1;
            }
        }
        if (direction == "Up")
        {
            for (int i = 0; i < length; i++)
            {
                aiBoard[XstartingPosition - i, YstartingPosition] = 1;
            }
        }
        if (direction == "Down")
        {
            for (int i = 0; i < length; i++)
            {
                aiBoard[XstartingPosition + i, YstartingPosition] = 1;
            }
        }
    } // המחשב ממקם את הצוללות שלו
    public bool CheckEndGame(int[,] arr)
    {
        int sum = 0;
        for (int i = 0; i < arr.GetLength(0); i++)
        {
            for (int j = 0; j < arr.GetLength(1); j++)
            {
                if (arr[i, j] == 3)
                {
                    sum++;
                }
            }
        }
        if (true)
        {
            return true;
        }
        return false;
    } // הפעולה בודקת אם המשחק נגמר
    public bool CheckCell(int[,] arr, int x, int y, string direction)
    {
        if (direction == "Right")
        {
            if (y + 1 > 9)
            {
                return false;
            }
        }
        if (direction == "Left")
        {
            if (y - 1 < 0)
            {
                return false;
            }
        }
        if (direction == "Up")
        {
            if (x - 1 < 0)
            {
                return false;
            }
        }
        if (direction == "Down")
        {
            if (x + 1 > 9)
            {
                return false;
            }
        }
        return true;
    } // הפעולה בודקת אם קיים תא במערך בכיוון הזה      
    public bool CheckDestroyedShip(int[,] arr, int row, int line)
    {
        int newLine = line;
        int newRow = row;
        if (arr[row, line] == 1)
        {
            return false;
        }
        while (CheckCell(arr, row, line, "Right") && arr[row, line + 1] != 0 && arr[row, line + 1] != 4)
        {
            if (arr[row, line + 1] == 1)
            {
                return false;
            }
            line++;
        }
        line = newLine;
        while (CheckCell(arr, row, line, "Left") && arr[row, line - 1] != 0 && arr[row, line - 1] != 4)
        {
            if (arr[row, line - 1] == 1)
            {
                return false;
            }
            line--;
        }
        line = newLine;
        while (CheckCell(arr, row, line, "Up") && arr[row - 1, line] != 0 && arr[row - 1, line] != 4)
        {
            if (arr[row - 1, line] == 1)
            {
                return false;
            }
            row--;
        }
        row = newRow;
        while (CheckCell(arr, row, line, "Down") && arr[row + 1, line] != 0 && arr[row + 1, line] != 4)
        {
            if (arr[row + 1, line] == 1)
            {
                return false;
            }
            row++;
        }
        return true;
    } // הפעולה בודקת אם הצוללת התפוצצה או לא
    public int RandomNumber(int[,] arr)
    {
        int sum = 0;
        for (int i = 0; i < arr.GetLength(0); i++)
        {
            for (int j = 0; j < arr.GetLength(1); j++)
            {
                if (arr[i, j] == 0 || arr[i, j] == 1)
                {
                    sum++;
                }
            }
        }
        System.Random rnd = new System.Random();
        return rnd.Next(0, sum) + 1;
    } // הפעולה נותנ מספר רנדומלי בין התאים הריקים בלוח
    public bool CheckZeros(int[,] arr, int x, int y, string direction, int length)
    {
        if (CheckDirection(arr, x, y, direction, length))
        {
            if (direction == "Right")
            {
                for (int i = 0; i < length; i++)
                {
                    if (arr[x, y + i] != 0)
                    {
                        return false;
                    }
                }
            }
            if (direction == "Left")
            {
                for (int i = 0; i < length; i++)
                {
                    if (arr[x, y - i] != 0)
                    {
                        return false;
                    }
                }
            }
            if (direction == "Up")
            {
                for (int i = 0; i < length; i++)
                {
                    if (arr[x - i, y] != 0)
                    {
                        return false;
                    }
                }
            }
            if (direction == "Down")
            {
                for (int i = 0; i < length; i++)
                {
                    if (arr[x + i, y] != 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        return false;
    } // הפעולה בודקת אם כל התאים שהשחקן בחר לשים בהם צוללת ריקים או לא
    public bool CheckDirection(int[,] arr, int x, int y, string direction, int length)
    {
        if (direction == "Right")
        {
            if (y + length > 10)
            {
                return false;
            }
        }
        if (direction == "Left")
        {
            if (y - length < -1)
            {
                return false;
            }
        }
        if (direction == "Up")
        {
            if (x - length < -1)
            {
                return false;
            }
        }
        if (direction == "Down")
        {
            if (x + length > 10)
            {
                return false;
            }
        }
        return true;
    } // הפעולה בודקת שאכן הכיוון שהשחקן רוצה להניח את הצוללת תקין 
    public int[] RandomArr()
    {
        System.Random rnd = new System.Random();
        int index = 0;
        int[] tempArr = { 1, 2, 3, 4 };
        int[] arr = new int[4];
        for (int i = tempArr.Length; i > 0; i--)
        {
            int number = rnd.Next(0, i);
            arr[index] = tempArr[number];
            tempArr[number] = 0;
            SetArr(tempArr);
            index++;
        }
        return arr;
    } // הפעולה מחזירה מערך של מספרים בסדר רנדומלי
    public void SetArr(int[] arr)
    {
        int index = 0;
        int[] newArr = new int[arr.Length];
        for (int i = 0; i < arr.Length; i++)
        {
            if (arr[i] != 0)
            {
                newArr[index] = arr[i];
                index++;
            }
        }
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = newArr[i];
        }
    } // הפעולה מסדרת מערך ככה שהתאים הריקים יהיו בסוף
    public void Sod()
    {
        if (ans == "Didnt hit a ship befor")
        {
            int randomPlace = RandomNumber(playerSecondBoard);
            for (int i = 0; i < playerSecondBoard.GetLength(0); i++) // המחשב בוחר מיקום  מהמקומת הרקים לזרוק בו פצצה
            {
                for (int j = 0; j < playerSecondBoard.GetLength(1); j++)
                {
                    if (playerSecondBoard[i, j] == 0 || playerSecondBoard[i, j] == 1)
                    {
                        randomPlace--;
                        if (randomPlace == 0)
                        {
                            computerRow = i;
                            computerLine = j;
                            i = playerSecondBoard.GetLength(0);
                            j = playerSecondBoard.GetLength(1);
                        }
                    }
                }
            }
            print(computerRow + " , " + computerLine);
            if (playerBoard[computerRow, computerLine] == 0) // אם המחשב החטיא
            {
                PlayerControllerScript.NodeHit(computerRow, computerLine, Color.yellow);

                playerSecondBoard[computerRow, computerLine] = 4;
                playerBoard[computerRow, computerLine] = 4;
            }
            if (playerBoard[computerRow, computerLine] == 1) // אם המחשב פגע
            {
                PlayerControllerScript.NodeHit(computerRow, computerLine, Color.red);

                playerSecondBoard[computerRow, computerLine] = 3;
                playerBoard[computerRow, computerLine] = 3;
                ans = "Right";
                count++;
            }


        }
        else
        {
            int newRow = computerRow;
            int newLine = computerLine;
            if (ans == "Right")
            {
                if (CheckCell(playerSecondBoard, newRow, newLine + (count - 1), ans) && playerSecondBoard[newRow, newLine + count] < 3)
                {
                    newLine += count;
                }
                else
                {
                    ans = "Left";
                    count = 1;
                }
            }
            if (ans == "Left")
            {
                if (CheckCell(playerSecondBoard, newRow, newLine - (count - 1), ans) && playerSecondBoard[newRow, newLine - count] < 3)
                {
                    newLine -= count;
                }
                else
                {
                    ans = "Up";
                    count = 1;
                }
            }
            if (ans == "Up")
            {
                if (CheckCell(playerSecondBoard, newRow - (count - 1), newLine, ans) && playerSecondBoard[newRow - count, newLine] < 3)
                {
                    newRow -= count;
                }
                else
                {
                    ans = "Down";
                    count = 1;
                }
            }
            if (ans == "Down")
            {
                if (CheckCell(playerSecondBoard, newRow + (count - 1), newLine, ans) && playerSecondBoard[newRow + count, newLine] < 3)
                {
                    newRow += count;
                }
                else
                {
                    count = 1;
                    ans = "Didnt hit a ship befor";
                }
            }
            if (playerBoard[newRow, newLine] == 0) // אם המחשב החטיא
            {
                PlayerControllerScript.NodeHit(newRow, newLine, Color.yellow);

                count = 1;
                playerSecondBoard[newRow, newLine] = 4;
                playerBoard[newRow, newLine] = 4;
                if (ans == "Right")
                {
                    ans = "Left";
                }
                else if (ans == "Left")
                {

                    {
                        ans = "Up";
                    }
                }
                else if (ans == "Up")
                {
                    ans = "Down";
                }
                else if (ans == "Down")
                {
                    ans = "Didnt hit a ship befor";
                }
            }
            if (playerBoard[newRow, newLine] == 1) // אם המחשב פגע
            {
                PlayerControllerScript.NodeHit(newRow, newLine, Color.red);
                playerSecondBoard[newRow, newLine] = 3;
                playerBoard[newRow, newLine] = 3;
                count++;
            }

            print(newRow + " , " + newLine);
        }
        if (CheckEndGame(playerBoard))
        {
            UI_Manager_Script.LoseGame();
        }
    }
    public void RegisterHit(Simple_Node Node)
    {
        if (GameManagerScript.GetActiveState() == GameManager.State.playerTurn)
        {
            int x, y;
            x = Node.name[Node.name.Length - 2] - '0';
            y = Node.name[Node.name.Length - 1] - '0';
            if (aiBoard[x, y] == 1)
            {
                aiSecondBoard[x, y] = 3;
                Node.SetColor(Color.red);
            }
            else
            {
                Node.SetColor(Color.yellow);
            }
            Node.GetComponent<BoxCollider>().enabled = false;
            Node.SetHit(true);
            if (true)
            {
                UI_Manager_Script.WinGame();
            }
            else
            {
                GameManagerScript.SetActiveState(GameManager.State.AI_Turn);
                UI_Manager_Script.SetTurnText("Turn: Enemy");
            }
        }
    }
}
