using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipCode : MonoBehaviour
{
    [SerializeField] private int size;
    [SerializeField] private bool IsDeployed;
    [SerializeField] private Vector3 LastPos;


    // Start is called before the first frame update
    void Start()
    {
        LastPos = transform.position;
        IsDeployed = false;
    }

    public int GetSize()
    {
        return size;
    }
    public bool GetIsDeployed()
    {
        return IsDeployed;
    }
    public void SetIsDeployed(bool value)
    {
        IsDeployed = value;
    }

    public Vector3 getLastPos()
    {
        return LastPos;
    }

}
